import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Reservas from '@/components/Reservas'
import Success from '@/components/Success'
import Ordenes from '@/components/Ordenes'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/reservas',
      name: 'Reservas',
      component: Reservas
    },
    {
      path: '/success',
      name: 'Success',
      component: Success
    },
    {
      path: '/ordenes',
      name: 'Ordenes',
      component: Ordenes
    }
  ]
})
